package com.xuexuehai.springcloud.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 1:58 下午
 */
@SpringBootApplication
public class QuartzApp {
    public static void main(String[] args) {
        SpringApplication.run(QuartzApp.class);
    }
}
