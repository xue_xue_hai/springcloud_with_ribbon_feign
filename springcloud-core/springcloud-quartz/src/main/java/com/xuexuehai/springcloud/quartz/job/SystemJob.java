package com.xuexuehai.springcloud.quartz.job;

import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 2:04 下午
 */
public abstract class SystemJob extends QuartzJobBean {

}
