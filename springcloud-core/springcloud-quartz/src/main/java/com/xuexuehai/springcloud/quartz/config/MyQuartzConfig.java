package com.xuexuehai.springcloud.quartz.config;

import com.xuexuehai.springcloud.quartz.job.SystemJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/8/23 3:07 下午
 */
@Configuration
public class MyQuartzConfig {

    @Bean
    public JobDetail jobDetail(){
        return JobBuilder
                .newJob(SystemJob.class)
                .withIdentity("read-usernames-from-db")
                .storeDurably(true)
                .build();
    }

    @Bean
    public Trigger jobTrigger(){
        return TriggerBuilder
                .newTrigger()
                .forJob(jobDetail())
                .withIdentity("read-usernames-from-db")
                .withSchedule(
                        CronScheduleBuilder
                                .cronSchedule("0 15 3 * * ? "))
                                //.cronSchedule("*/3 * * * * ?"))
                .build();
    }
}
