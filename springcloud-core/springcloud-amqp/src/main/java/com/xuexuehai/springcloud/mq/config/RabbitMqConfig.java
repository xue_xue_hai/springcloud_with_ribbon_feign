package com.xuexuehai.springcloud.mq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/8/25 2:55 下午
 */
@Configuration
public class RabbitMqConfig {
    /**
     * 定义Exange名称
     */
    public static final String TOPIC_EXCHANGE = "my_topic_exchange";

    /**
     * 定义topic queue名称
     */
    public static final String TOPIC_QUEUE_A = "queue_a";
    public static final String TOPIC_QUEUE_B = "queue_b";

    /**
     * 定义路由规则 ROUTING_KEY
     */
    public static final String ROUTING_KEY_A = "A.topic.*";
    public static final String ROUTING_KEY_B = "*.topic.*";

    @Autowired
    private QueueConfig queueConfig;
    @Autowired
    private ExchangeConfig exchangeConfig;

    /**
     * 绑定Exchange
     */
    @Bean
    public Binding bindingTopicA(){
        return BindingBuilder.bind(queueConfig.topicQueueA())
                .to(exchangeConfig.topicExchange())
                .with(RabbitMqConfig.ROUTING_KEY_A);
    }
    @Bean
    public Binding bindingTopicB(){
        return BindingBuilder.bind(queueConfig.topicQueueB())
                .to(exchangeConfig.topicExchange())
                .with(RabbitMqConfig.ROUTING_KEY_B);
    }

}
