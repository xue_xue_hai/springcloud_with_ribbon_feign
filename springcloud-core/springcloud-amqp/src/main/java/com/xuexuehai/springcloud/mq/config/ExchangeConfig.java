package com.xuexuehai.springcloud.mq.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/8/25 2:56 下午
 */

@Configuration
public class ExchangeConfig {
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(RabbitMqConfig.TOPIC_EXCHANGE,true,false);
    }
}
