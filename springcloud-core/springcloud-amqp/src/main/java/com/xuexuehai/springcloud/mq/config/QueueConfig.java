package com.xuexuehai.springcloud.mq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/8/25 2:56 下午
 */
@Configuration
public class QueueConfig {
    @Bean
    public Queue topicQueueA(){
        return new Queue(RabbitMqConfig.TOPIC_QUEUE_A,true,false,false);
    }

    @Bean
    public Queue topicQueueB(){
        return new Queue(RabbitMqConfig.TOPIC_QUEUE_B,true,false,false);
    }
}
