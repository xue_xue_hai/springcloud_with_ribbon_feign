package com.xuexuehai.springcloud.common.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 11:30 上午
 *
 * 带有建造者模式的DTO
 */
@Getter
@ToString
@NoArgsConstructor
public class HttpResp {
    private int code;
    private String msg;
    private Object results;
    private LocalDateTime times;

    public HttpResp(Builder builder){
        this.code = builder.code;
        this.msg = builder.msg;
        this.results = builder.results;
        this.times = builder.times;
    }

    public static class Builder{
        private int code;
        private String msg;
        private Object results;
        private LocalDateTime times;

        public Builder code(int code){
            this.code = code;
            return this;
        }

        public Builder msg(String msg){
            this.msg = msg;
            return this;
        }

        public Builder results(Object results){
            this.results = results;
            return this;
        }

        public Builder times(LocalDateTime times){
            this.times = times;
            return this;
        }

        public HttpResp builder(){
            return new HttpResp(this);
        }
    }
}
