package com.xuexuehai.springcloud.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 11:29 上午
 */
@SpringBootApplication
public class CommonApp {
    public static void main(String[] args) {
        SpringApplication.run(CommonApp.class);
    }
}
