package com.xuexuehai.springcloud.common.exception;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 1:40 下午
 *
 * 系统异常
 */
public class SystemException extends RuntimeException{
    public SystemException(String message) {
        super(message);
    }
}
