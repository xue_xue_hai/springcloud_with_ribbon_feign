package com.xuexuehai.springcloud.common.config.mvc;

import lombok.SneakyThrows;
import org.apache.tomcat.jni.Local;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 11:50 上午
 */
@Configuration
public class CommonMvcConfiguration implements WebMvcConfigurer {

    private final static ThreadLocal<SimpleDateFormat> LOCAL = new ThreadLocal<>();

    @Override
    public void addFormatters(FormatterRegistry registry) {
        final SimpleDateFormat sdf = new SimpleDateFormat();
        registry.addConverter(new Converter<String, Date>() {
            @SneakyThrows
            @Override
            public Date convert(String s) {
                if(LOCAL.get() == null){
                    LOCAL.set(sdf);
                }
                SimpleDateFormat simpleDateFormat = LOCAL.get();
                Date date = simpleDateFormat.parse(s);
                return date;
            }
        });
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("GET","POST","PUT","DELETE")
                .allowCredentials(true)
                //60分钟内允许跨域
                .maxAge(60*60);
    }
}
