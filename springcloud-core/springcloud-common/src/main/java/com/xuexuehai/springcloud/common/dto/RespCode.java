package com.xuexuehai.springcloud.common.dto;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 11:30 上午
 */
public enum RespCode {

    /**
     * code：20001 <br/>
     * msg: 添加图书成功
     */
    ADD_BOOK_SUCCESS(20001,"添加图书成功"),
    /**
     * code：20002 <br/>
     * msg: 查询所有图书成功
     */
    FIND_ALL_BOOKS_SUCCESS(20002,"查询所有图书成功"),

    /**
     * code：40001 <br/>
     * msg: 微服务访问异常
     */
    FALLBACK_ERROR(40001,"微服务访问异常")
    ;

    RespCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
