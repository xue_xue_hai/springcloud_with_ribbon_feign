项目架构
![structure](other/imgs/structure.jpg)
项目pom配置
1. books.pom
 - springboot `<parent>`
 - springcloud `<dependencyManagement>`
 - build插件 `maven-compiler-plugin`
 - dependencies `lombok`
 - `spring-boot-starter-web`
2. base
 - 添加网关模块
 - eureka.pom 添加eureka server 依赖
3. bm
 - bm.pom 添加eureka client 依赖
 - bm.pom 添加 `spring-boot-maven-plugin`
   - bm-ribbon.pom 添加 `spring-cloud-starter-netflix-ribbon` 依赖
 - bm-feign.pom 添加 `spring-cloud-starter-openfeign` 依赖
4. core
 - springcloud-amqp 添加 `spring-boot-starter-amqp` 依赖
 - springcloud-cache 添加 `spring-boot-starter-data-redis` 依赖
5. domain
 - jpa
 - @SpringBootApplication(exclude={数据源的自动配置类})

ribbon
 * 依赖ribbon
   * RestTemplate: @LoadBalance
     * 线性轮询（默认）
     * 随机
     * 加权轮询
     * 服务器连接数、响应时间等
   * 问题
     * ribbon本质上还是客户端--EurekaClient-->注册
     * book微服务：api(controller): /api/book/findAllBook
       * book微服务部署在不同机器（服务器上）
       * 用户微服务：查询所有图书
         * 没有查询所有图书的职责
         * 向book发起微服务请求
           * 用户微服务并不知道book微服务部署在哪台机器上，只知道它注册中注册的微服务名称-->springcloudbook
           * 发起请求，http://微服务名称/api/book/findAllBook
             * 解析发现有n台机器部署了微服务`治理中心不知道用户需要使用哪个机器上提供的服务`
             * 通过用户微服务器有ribbon设置，使用默认的轮询算法对这n台机器进行轮询访问
               * 第一次：可能访问1号机
               * 第二次：可能访问2号机
               * ....
 
 feign
 * 声明了一个接口

 ```java
import org.springframework.web.bind.annotation.PutMapping;

@Feignclinet(value = "微服务名称", path = "微服务路径(如：/api/book)")
public interface FeignApi() {
   @GetMapping("/findAllBook")
   HttpResp findAllBook();

   @PutMapping("/addBook")
   HttpResp addBook();
}
```
 * Controller: 注入接口

ribbon和feign的区别
 * feign使用是声明式接口、ribbon使用是类的实例化
 * feign封装了ribbon

熔断机制 - hystrix
1. 引入Hystrix组件
2. 配置application.yml文件 feign. hystrix.enable=true

网关
1. `@EnableZuulProxy //代理对象启动网关`