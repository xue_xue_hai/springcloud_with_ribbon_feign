package com.xuexuehai.springcloud.domain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 11:19 上午
 */
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class
})
public class DomainApp {
    public static void main(String[] args) {
        SpringApplication.run(DomainApp.class);
    }
}
