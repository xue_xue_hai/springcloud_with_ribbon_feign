package com.xuexuehai.springcloud.book.service.impl;

import com.xuexuehai.springcloud.book.service.IBookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 2:56 下午
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class BookServiceImplTest {

    @Autowired
    private IBookService ibs;

    @Test
    public void findAllBook() {
        System.out.println(ibs.findAllBook());
    }
}