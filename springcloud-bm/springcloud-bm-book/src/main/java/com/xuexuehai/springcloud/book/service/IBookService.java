package com.xuexuehai.springcloud.book.service;

import com.xuexuehai.springcloud.domain.entity.Book;

import java.util.List;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 2:39 下午
 */
public interface IBookService {
    List<Book> findAllBook();
}
