package com.xuexuehai.springcloud.book.dao;

import com.xuexuehai.springcloud.domain.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 2:36 下午
 */
@Repository
public interface IBookDao extends JpaRepository<Book,Integer> {
}
