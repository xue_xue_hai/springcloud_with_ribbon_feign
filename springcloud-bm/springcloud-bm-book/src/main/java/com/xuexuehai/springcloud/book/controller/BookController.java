package com.xuexuehai.springcloud.book.controller;

import com.xuexuehai.springcloud.book.service.IBookService;
import com.xuexuehai.springcloud.common.dto.HttpResp;
import com.xuexuehai.springcloud.common.dto.RespCode;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 3:00 下午
 */
@RestController
@RequestMapping("/api/book")
public class BookController {

    @Value("${server.port}")
    private int port;

    @Autowired
    private IBookService ibs;

    @GetMapping("/findAllBook")
    @ApiOperation("图书管理子模块")
    public HttpResp findAllBook(){


        return  new HttpResp.Builder()
                .code(RespCode.FIND_ALL_BOOKS_SUCCESS.getCode())
                .msg(RespCode.FIND_ALL_BOOKS_SUCCESS.getMsg() + "-port" + port)
                .results(ibs.findAllBook())
                .times(LocalDateTime.now())
                .builder();
    }
}
