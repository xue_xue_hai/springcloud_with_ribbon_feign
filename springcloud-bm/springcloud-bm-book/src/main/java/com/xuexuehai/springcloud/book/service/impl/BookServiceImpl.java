package com.xuexuehai.springcloud.book.service.impl;

import com.netflix.discovery.converters.Auto;
import com.xuexuehai.springcloud.book.dao.IBookDao;
import com.xuexuehai.springcloud.book.service.IBookService;
import com.xuexuehai.springcloud.domain.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;
import java.util.List;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 2:45 下午
 */
@Service
@Transactional
public class BookServiceImpl implements IBookService {

    @Autowired
    private IBookDao iBookDao;

    @Override
    public List<Book> findAllBook() {
        return iBookDao.findAll();
    }
}
