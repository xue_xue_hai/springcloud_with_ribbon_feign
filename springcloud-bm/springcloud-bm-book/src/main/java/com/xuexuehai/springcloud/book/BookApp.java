package com.xuexuehai.springcloud.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 2:32 下午
 */
@EntityScan(basePackages = "com.xuexuehai.springcloud.domain.entity")
@SpringBootApplication
@EnableEurekaClient
@EnableOpenApi
public class BookApp {
    public static void main(String[] args) {
        SpringApplication.run(BookApp.class);
    }
}
