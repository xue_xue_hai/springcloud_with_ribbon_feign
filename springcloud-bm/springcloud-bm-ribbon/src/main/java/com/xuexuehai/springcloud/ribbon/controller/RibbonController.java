package com.xuexuehai.springcloud.ribbon.controller;

import com.xuexuehai.springcloud.common.dto.HttpResp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 4:03 下午
 */
@Api("ribbon RPC 远程访问book模块信息")
@RequestMapping("/api/ribbon")
@RestController
public class RibbonController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/findAll")
    @ApiOperation("ribbon负载均衡访问远程微服务book，获取book微服务返回的所有图书信息")
    public HttpResp findAll(){
        HttpResp body = restTemplate.getForEntity("http://springcloud-book/api/book/findAllBook", HttpResp.class)
                .getBody();
        return body;
    }
}
