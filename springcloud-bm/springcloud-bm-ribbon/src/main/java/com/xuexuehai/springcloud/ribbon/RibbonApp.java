package com.xuexuehai.springcloud.ribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 4:01 下午
 */
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class
})
@EnableEurekaClient
public class RibbonApp {
    public static void main(String[] args) {
        SpringApplication.run(RibbonApp.class);
    }
}
