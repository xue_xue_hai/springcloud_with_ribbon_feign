package com.xuexuehai.springcloud.feign.hystrix;

import com.xuexuehai.springcloud.common.dto.HttpResp;
import com.xuexuehai.springcloud.common.dto.RespCode;
import com.xuexuehai.springcloud.feign.api.FeignApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/5 10:32 上午
 *
 * 服务熔断的降级处理类
 */
@Slf4j
@Component
public class MyFallBackFactory implements FallbackFactory<FeignApi> {

    @Override
    public FeignApi create(Throwable throwable) {
        return new FeignApi() {
            @Override
            public HttpResp feignFindAll() {
                log.info("服务出现熔断" + throwable.getMessage());
                return new HttpResp.Builder()
                        .code(RespCode.FALLBACK_ERROR.getCode())
                        .msg(RespCode.FALLBACK_ERROR.getMsg())
                        .results(null)
                        .times(LocalDateTime.now())
                        .builder();
            }
        };
    }
}
