package com.xuexuehai.springcloud.feign.api;

import com.xuexuehai.springcloud.common.dto.HttpResp;
import com.xuexuehai.springcloud.feign.hystrix.MyFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 4:54 下午
 *
 * 通过反射获取FeignClient注解中指定的fallbackFactory的类中定义的方法处理服务无法访问的情况
 */

@FeignClient(value = "springcloud-book",path = "/api/book",fallbackFactory = MyFallBackFactory.class)
public interface FeignApi {

    /**
     * 要和使用的BookController的请求路径相同
     * @return HttpResp
     */
    @RequestMapping("/findAllBook")
    HttpResp feignFindAll();
}
