package com.xuexuehai.springcloud.feign.controller;

import com.xuexuehai.springcloud.common.dto.HttpResp;
import com.xuexuehai.springcloud.feign.api.FeignApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xuexuehai
 * @mailBox xuehai.xue@QQ.com
 * @date 2021/9/4 4:53 下午
 */
@Api("Feign访问微服务")
@RestController
@RequestMapping("/api/feign")
public class FeignController {
    @Autowired
    private FeignApi api;


    /**
     * 暴露在外面的测试swagger访问路径
     * @return HttpResp
     */
    @ApiOperation("feign负载均衡，获得微服务book返回的所有书籍")
    @GetMapping("/findAllBook")
    public HttpResp findAll(){
        return api.feignFindAll();
    }
}
